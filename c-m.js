{
  const {
    html,
  } = Polymer;
  /**
    `<c-m>` Description.

    Example:

    ```html
    <c-m></c-m>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --c-m | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CM extends Polymer.Element {

    static get is() {
      return 'c-m';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="c-m-styles c-m-shared-styles"></style>
      <slot></slot>
      
          <p>Welcome to Cells</p>
      
      `;
    }
  }

  customElements.define(CM.is, CM);
}